package hosein.videoplayerfinal;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.VideoView;

import static android.R.attr.path;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
  private VideoView videoview;
  public SeekBar slider;
  private static Handler handler = new Handler();
  private Uri path;

  //------------------
  private void updateProgressBar() {
    handler.postDelayed(updateTimeTask, 100);
  }

  private Runnable updateTimeTask = new Runnable() {
    public void run() {
      slider.setProgress(videoview.getCurrentPosition());
      slider.setMax(videoview.getDuration());
      handler.postDelayed(this, 100);
    }
  };
//---------------------
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    path = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.film);

    // Execute StreamVideo AsyncTask

//-----------------
    videoview = (VideoView) findViewById(R.id.mainVideoView);

    slider = (SeekBar) findViewById(R.id.slider);
    slider.setOnSeekBarChangeListener(this);
    videoview.setVideoURI(path);
    videoview.start();
    updateProgressBar();

  }

  @Override
  public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    if(fromUser) {
      videoview.seekTo(progress*1000);
    }


  }

  @Override
  public void onStartTrackingTouch(SeekBar seekBar) {
    handler.removeCallbacks(updateTimeTask);
  }

  @Override
  public void onStopTrackingTouch(SeekBar seekBar) {
    handler.removeCallbacks(updateTimeTask);
    videoview.seekTo(slider.getProgress());
    updateProgressBar();
  }


  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
  }

}
